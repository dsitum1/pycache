from setuptools import setup

setup(
    name='pycache',
    version='0.1',
    description='Basic cache library with support for various cache adapters',
    url='https://bitbucket.org/dsitum1/pycache',
    author='Domagoj Situm',
    author_email='domagoj.situm@gmail.com',
    license='MIT',
    packages=['pycache'],
    install_requires=[
        'six>=1'
        'singleton>=0.1.0'
    ],
    zip_safe=False
)
