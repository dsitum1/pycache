from singleton.singleton import Singleton
from ..utils import NullObject
from ..adapters import BaseAdapter
from ..errors import InvalidMessageAdapter


@Singleton
class Cache(object):
    def __init__(self, duration=3600, adapter=None):
        """
        Create new cache class
        :param duration: Duration (in seconds) of cached items
        """
        self._adapter = None
        self.duration = duration
        self._empty_object = NullObject.instance()
        if adapter:
            self.adapter(adapter)

    def adapter(self, adapter):
        """
        Sets or updates cache adapter, e.g. RedisAdapter, MemoryAdapter, etc.
        :param adapter:
        """
        if not isinstance(adapter, BaseAdapter):
            raise InvalidMessageAdapter(adapter)
        if self._adapter:
            self._adapter.shutdown()
        self._adapter = adapter
        return self

    def get(self, key, default=None):
        """
        Get item from cache by key. Return default value if item not found
        :param key:     Unique identifier of the key
        :param default: Default value returned if item is not found
        """
        data = self._adapter.get(key)
        return data if data is not self._empty_object else default

    def invalidate(self, key):
        """
        Removes key from cache
        :param key:
        """
        self._adapter.invalidate(key)
        return self

    def invalidate_all(self, startswith=None):
        """
        Removes all keys from cache
        :param startswith: If provided, returns only keys which start with given string
        """
        self._adapter.invalidate_all(startswith)
        return self

    def set(self, key, value, duration=None):
        """
        Set item to cache.
        :param key:         Unique identifier of the key
        :param value:       Item to cache
        :param duration:    Duration (in seconds) of cached items. Overrides default value if provide
        :return:
        """
        if duration is None:
            duration = self.duration
        self._adapter.set(key, value, duration)
        return self

    def memoize(self, key, duration=None):
        """
        Decorator which caches output of specified function
        :param key:         Key to identify result of cached function.
        :param duration:    Optional. Duration (in seconds) of cached items. Overrides default value if provided
        :return:
        """
        if duration is None:
            duration = self.duration

        def real_decorator(fn):
            def wrapper(*args, **kwargs):
                formatted_key = key.format(*(args[1:]))
                data = self._adapter.get(formatted_key)
                if data is self._empty_object:
                    data = fn(*args, **kwargs)
                    self._adapter.set(formatted_key, data, duration)
                return data

            return wrapper

        return real_decorator
