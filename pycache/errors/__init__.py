

class InvalidMessageAdapter(Exception):
    def __init__(self, adapter):
        msg = 'Provided adapter {} is not instance of BaseAdapter class'.format(adapter)
        super(InvalidMessageAdapter, self).__init__(msg)
