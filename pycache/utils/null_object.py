from singleton.singleton import Singleton


@Singleton
class NullObject(object):
    pass
