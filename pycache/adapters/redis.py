from . import BaseAdapter


class RedisAdapter(BaseAdapter):
    def __init__(self, host, port, prefix=''):
        raise NotImplementedError('not yet implemented')

    def get(self, key):
        pass

    def set(self, key, value, default):
        pass

    def invalidate(self, key):
        pass

    def invalidate_all(self, starts_with=None):
        pass

    def shutdown(self):
        pass
