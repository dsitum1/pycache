from ..utils import NullObject


class BaseAdapter(object):
    _empty_object = NullObject.instance()

    def get(self, key):
        """
        Returns found object or BaseAdapter._empty_object if entity with given key not found
        """
        raise NotImplementedError('method not implemented')

    def set(self, key, value, duration):
        raise NotImplementedError('method not implemented')

    def invalidate(self, key):
        raise NotImplementedError('method not implemented')

    def invalidate_all(self, starts_with=None):
        raise NotImplementedError('method not implemented')

    def shutdown(self):
        """
        Kills adapter and free up memory
        """
        raise NotImplementedError('method not implemented')
