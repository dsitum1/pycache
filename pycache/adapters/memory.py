from threading import Lock, Thread
from time import time, sleep
from .base import BaseAdapter
from six import iteritems


class MemoryAdapter(BaseAdapter):
    def __init__(self, prune_interval_seconds=60):
        super(MemoryAdapter, self).__init__()
        self._cache = {}
        self._lock = Lock()
        self._kill_thread = False
        self._cleanup_thread = Thread(target=self._prune, args=(prune_interval_seconds,))
        self._cleanup_thread.daemon = True
        self._cleanup_thread.start()

    def get(self, key):
        self._lock.acquire()
        record = self._cache.get(key, None)
        data = self._empty_object
        if record is not None:
            if record[0] < time():
                del self._cache[key]
            else:
                data = record[1]
        self._lock.release()
        return data

    def set(self, key, value, duration):
        if value is None:
            return
        self._lock.acquire()
        self._cache[key] = (time() + duration, value)
        self._lock.release()

    def invalidate(self, key):
        self._lock.acquire()
        self._cache.pop(key, None)
        self._lock.release()

    def invalidate_all(self, starts_with=None):
        self._lock.acquire()
        if starts_with is None:
            self._cache = {}
            self._lock.release()
            return

        # get keys to delete
        to_remove = []
        for key in self._cache:
            if key.startswith(starts_with):
                to_remove.append(key)
        # delete target keys
        for key in to_remove:
            del self._cache[key]
        self._lock.release()

    def _prune(self, interval):
        while self._kill_thread is False:
            self._lock.acquire()
            now = time()
            # get expired keys
            to_remove = []
            for key, record in iteritems(self._cache):
                if record[0] < now:
                    to_remove.append(key)
            # delete expired items
            for key in to_remove:
                del self._cache[key]
            self._lock.release()
            sleep(interval)

    def shutdown(self):
        self.invalidate_all()
        self._kill_thread = True
